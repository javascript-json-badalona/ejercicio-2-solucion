let fraseCamelCase = 'holaBienvenidosAlCursoDeJS';
let fraseSnakeCase = '';

for (let i = 0; i < fraseCamelCase.length; i++) {
  let letra = fraseCamelCase[i];
  if (letra === letra.toUpperCase()) {
    fraseSnakeCase += '_' + letra.toLowerCase();
  } else {
    fraseSnakeCase += letra;
  }
}

console.log(fraseSnakeCase);
